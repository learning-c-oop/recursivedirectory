﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RecursiveDirectory
{
    class Program
    {
        static void Main(string[] args)
        {
            var dir = new DirectoryInfo(@"G:\cragravorum\c#\gitlab\Learning C# OOP\RecursiveDirectory\RecursiveDirectory");
            PrintDirectories(dir);

            Console.ReadKey();
        }
        static void PrintDirectories(DirectoryInfo di)
        {
            Console.WriteLine();
            var dis = di.GetDirectories();
            foreach (var item in dis)
            {
                Console.WriteLine(item.FullName);
                PrintDirectories(item);
            }
        }
    }
}
